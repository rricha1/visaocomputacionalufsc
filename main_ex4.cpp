#define	 USE_VIDEO

#include "tracking_aux.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <stdio.h>

int main(void)
{
    // Tracking parameters
    std::string	filename = "/path/to/video";

    int		isgrayscale = 1;

    double	clock_start,
            clock_stop,
            tick = cvGetTickFrequency();

    cv::Mat ICur,
            ICur_raw;

    // Loading video
    printf("> Loading video ...\n");

#ifdef USE_VIDEO
    cv::VideoCapture capture(0);
#else
    cv::VideoCapture capture(filename);
#endif

    if(!capture.isOpened())
    {
        printf("* Ops! what happened to the video?");
        exit(0);
    }
    printf("> Video loaded...\n");

    // Sempre uso grayscale
    isgrayscale = 1;

    // loop
    char key1 = 0;
    while(key1 != 'q' && key1 != 'Q')
    {
        // Grabs image from video
        GetFrame(&capture, &ICur, &ICur_raw, isgrayscale);

        cv::Mat padded;
        ICur.convertTo(padded, CV_32F);

//        cv::Mat padded;                            //expand input image to optimal size
//        int m = cv::getOptimalDFTSize( ICur.rows );
//        int n = cv::getOptimalDFTSize( ICur.cols ); // on the border add zero values
//        cv::copyMakeBorder(ICur, padded, 0, m - ICur.rows, 0, n - ICur.cols, cv::BORDER_CONSTANT, cv::Scalar::all(0));

        cv::Mat magnitude(ICur.rows, ICur.cols, CV_32FC1);
        cv::Mat phase(ICur.rows, ICur.cols, CV_32FC1);

        // here we just copy the padded image into complexI (through this rather complicated succession of merges)
//        cv::Mat planes[] = {cv::Mat_<float>(padded), cv::Mat::zeros(padded.size(), CV_32F)};
//        cv::Mat complexI;
//        cv::merge(planes, 2, complexI);         // Add to the expanded another plane with zeros

//        cv::dft(complexI, complexI);            // this way the result may fit in the source matrix

        cv::Mat fourierTransform;
        cv::dft(padded, fourierTransform,  cv::DFT_COMPLEX_OUTPUT);

        // compute the magnitude and switch to logarithmic scale
        // => log(1 + sqrt(Re(DFT(I))^2 + Im(DFT(I))^2))
//        cv::split(complexI, planes);                   // planes[0] = Re(DFT(I), planes[1] = Im(DFT(I))
        cv::Mat planes[2];
        cv::split(fourierTransform, planes);

        cv::magnitude(planes[0], planes[1], magnitude); // planes[0] = magnitude
        cv::Mat magI = magnitude;
        magI += cv::Scalar::all(1);                    // switch to logarithmic scale
        cv::log(magI, magI);

        // computes the phase
        cv::phase(planes[0], planes[1], phase);
        phase += cv::Scalar::all(1);
        cv::log(phase, phase);

//        // crop the spectrum, if it has an odd number of rows or columns
//        magI = magI(cv::Rect(0, 0, magI.cols & -2, magI.rows & -2));
//        phase = phase(cv::Rect(0, 0, phase.cols & -2, phase.rows & -2));

////        // rearrange the quadrants of Fourier image  so that the origin is at the image center
////        int cx = magI.cols/2;
////        int cy = magI.rows/2;

////        // first for magnitude image
////        cv::Mat q0(magI, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
////        cv::Mat q1(magI, cv::Rect(cx, 0, cx, cy));  // Top-Right
////        cv::Mat q2(magI, cv::Rect(0, cy, cx, cy));  // Bottom-Left
////        cv::Mat q3(magI, cv::Rect(cx, cy, cx, cy)); // Bottom-Right

////        cv::Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
////        q0.copyTo(tmp);
////        q3.copyTo(q0);
////        tmp.copyTo(q3);

////        q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
////        q2.copyTo(q1);
////        tmp.copyTo(q2);

////        // then for magnitude image
////        cv::Mat r0(phase, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
////        cv::Mat r1(phase, cv::Rect(cx, 0, cx, cy));  // Top-Right
////        cv::Mat r2(phase, cv::Rect(0, cy, cx, cy));  // Bottom-Left
////        cv::Mat r3(phase, cv::Rect(cx, cy, cx, cy)); // Bottom-Right

////        r0.copyTo(tmp);
////        r3.copyTo(r0);
////        tmp.copyTo(r3);

////        r1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
////        r2.copyTo(r1);
////        tmp.copyTo(r2);

        cv::normalize(magI, magI, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
                                                // viewable image form (float between values 0 and 1).

        cv::normalize(phase, phase, 0, 1, CV_MINMAX); // Transform the matrix with float values into a
                                                // viewable image form (float between values 0 and 1).

        // Displaying shifted fourier spectrum
        cv::imshow("spectrum magnitude", magI);
        cv::imshow("spectrum phase", phase);


        cv::cartToPolar(planes[0], planes[1], magnitude, phase);

        // Some operations on the fourier spectrum!
        //        for(int i=0;i<magnitude.rows;i++)
        //            for(int j=0;j<magnitude.cols;j++)
        //                if(magnitude.at<float>(i,j)>8000)
        //                    1==1;//std::cout << magnitude.at<float>(i,j) << std::endl;
        //                 else
        //                    magnitude.at<float>(i,j) = 0;


        int fraction = 20;

        for(int i=cvRound((double)magnitude.rows/fraction);i<cvRound((double)(fraction-1)*magnitude.rows/fraction);i++)
            for(int j=cvRound((double)magnitude.cols/fraction);j<cvRound((double)(fraction-1)*magnitude.cols/fraction);j++)
                    magnitude.at<float>(i,j) = 0;

        cv::imshow("teste", magnitude);
        //


        cv::polarToCart(magnitude, phase, planes[0], planes[1]);

        cv::merge(planes, 2, fourierTransform);

        std::cout << "Inverse transform...\n";
        cv::Mat inverseTransform;
        cv::dft(fourierTransform, inverseTransform,  cv::DFT_SCALE | cv::DFT_INVERSE | cv::DFT_REAL_OUTPUT);

        cv::Mat finalImage;
        inverseTransform.convertTo(finalImage, CV_8U);


        // Display
        cv::imshow("Input Image"       , ICur   );    // Show the result
        cv::imshow("Output Image"       , finalImage );    // Show the result
        cv::Mat difference = ICur-finalImage;
        cv::imshow("difference", difference);

        key1 = cv::waitKey(10);

        // Clock start/stop
        clock_stop = ((double)cvGetTickCount()-clock_start)/(1000*tick);
        clock_start = (double)cvGetTickCount();

        // Terminal
        printf("> Elapsed: %f ms\n", clock_stop);
    }
}
