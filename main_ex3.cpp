#define	 USE_VIDEO

#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "tracking_aux.h"


int main(void)
{
    // Tracking parameters
    std::string	filename = "/path/to/video";

    int		isgrayscale = 1;

    double	clock_start,
            clock_stop,
            tick = cvGetTickFrequency();

    cv::Mat ICur,
            ICur_raw;

    // Loading video
    printf("> Loading video ...\n");

#ifdef USE_VIDEO
    cv::VideoCapture capture(0);
#else
    cv::VideoCapture capture(filename);
#endif

    if(!capture.isOpened())
    {
        printf("* Ops! what happened to the video?");
        exit(0);
    }
    printf("> Video loaded...\n");

    // Definindo filtros aqui
    cv::Size size_kernel(20,20);
    cv::Point anchor(-1,-1);
    int ddepth = -1;
    cv::Mat kernel(9,9,CV_32FC1);
    kernel.setTo(1);
    kernel /= 81;

    // loop
    char key1 = 0;
    while(key1 != 'q' && key1 != 'Q')
    {
        // Grabs image from video
        GetFrame(&capture, &ICur, &ICur_raw, isgrayscale);

        // Filtro generico
        cv::filter2D(ICur, ICur, ddepth, kernel, anchor, 0, cv::BORDER_REPLICATE);

        // Display
        cv::imshow("Janela2", ICur);
        key1 = cv::waitKey(10);

        // Clock start/stop
        clock_stop = ((double)cvGetTickCount()-clock_start)/(1000*tick);
        clock_start = (double)cvGetTickCount();

        // Terminal
        printf("> Elapsed: %f ms\n", clock_stop);
    }
}
