#include <cstdio>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

// Tracking aux fcts

void	OnMouse(int event, int x, int y, int, void* test);

void	GetFrame(cv::VideoCapture *capture, cv::Mat *ICur, cv::Mat *ICur_raw, int isgrayscale);
