#include "tracking_aux.h"

void	GetFrame(cv::VideoCapture *capture, cv::Mat *ICur, cv::Mat *ICur_raw, int isgrayscale)
{
    *capture >> *ICur_raw;

    if(ICur_raw->channels() > 1 && isgrayscale == 1)
    {
        cv::cvtColor(*ICur_raw, *ICur, CV_BGR2GRAY);
    }
    else
        *ICur = *ICur_raw;
}

void	OnMouse(int event,
				int x,
				int y,
				int,
				void* test)
{
	int *mouse_coords = (int*) test;

	if(event == CV_EVENT_LBUTTONDOWN)
	{
		mouse_coords[0] = x;
		mouse_coords[1] = y;

		printf("Coords selected!\n");
	}    
}

