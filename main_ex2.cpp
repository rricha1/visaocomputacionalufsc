#define	 USE_VIDEO

#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "tracking_aux.h"


int main(void)
{
	// Tracking parameters
    std::string	filename = "/path/to/video";

    int		isgrayscale = 1;

	double	clock_start,
			clock_stop,
			tick = cvGetTickFrequency();
			
	cv::Mat ICur,
            ICur_raw;

    // Loading video
    printf("> Loading video ...\n");

#ifdef USE_VIDEO
    cv::VideoCapture capture(0);
#else
    cv::VideoCapture capture(filename);
#endif

    if(!capture.isOpened())
    {
        printf("* Ops! what happened to the video?");
        exit(0);
    }
    printf("> Video loaded...\n");

    // Definindo filtros aqui
    cv::Size size_kernel(20,20);
    cv::Point anchor(-1,-1);

    // loop
    char key1 = 0;
    while(key1 != 'q' && key1 != 'Q')
    {
		// Grabs image from video
        GetFrame(&capture, &ICur, &ICur_raw, isgrayscale);

        // Filtragem Blur
        //cv::blur(ICur, ICur, size_kernel, anchor, cv::BORDER_REPLICATE);

        // Filtragem Gaussiana
        double sigmax = 1;
        double sigmay = 1;
        cv::GaussianBlur(ICur, ICur, size_kernel, sigmax, sigmay, cv::BORDER_REPLICATE);

        // Display
        cv::imshow("Janela2", ICur);
        key1 = cv::waitKey(10);

		// Clock start/stop
		clock_stop = ((double)cvGetTickCount()-clock_start)/(1000*tick);
		clock_start = (double)cvGetTickCount();
		
		// Terminal
        printf("> Elapsed: %f ms\n", clock_stop);
	}
}
