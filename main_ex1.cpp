#define	 USE_VIDEO

#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "tracking_aux.h"



int main(void)
{
	// Tracking parameters
    std::string	filename = "/path/to/video";

    int		isgrayscale = 1;
    int     kernel_dilation = 20,
            kernel_erosion = 20;

	double	clock_start,
			clock_stop,
			tick = cvGetTickFrequency();
			
	cv::Mat ICur,
            ICur_raw;

    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3,3));

    // Loading video
    printf("> Loading video ...\n");

#ifdef USE_VIDEO
    cv::VideoCapture capture(0);
#else
    cv::VideoCapture capture(filename);
#endif

    if(!capture.isOpened())
    {
        printf("* Ops! what happened to the video?");
        exit(0);
    }
    printf("> Video loaded...\n");
		
    // loop
    char key1 = 0;
    while(key1 != 'q' && key1 != 'Q')
    {
		// Grabs image from video
		GetFrame(&capture, &ICur, &ICur_raw, isgrayscale);

        // Opera�ao
        cv::dilate(ICur, ICur, kernel, cv::Point(-1,-1), kernel_dilation);
        cv::erode(ICur, ICur, kernel, cv::Point(-1,-1), kernel_erosion);

        // Display
        cv::imshow("Janela", ICur);
        key1 = cv::waitKey(10);

		// Clock start/stop
		clock_stop = ((double)cvGetTickCount()-clock_start)/(1000*tick);
		clock_start = (double)cvGetTickCount();
		
		// Terminal
        printf("> Elapsed: %f ms\n", clock_stop);
	}
}
